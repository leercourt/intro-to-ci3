# README #
![Scheme](/Images/Edge_Logo.jpg)!

Edge Academy: Introduction to Continuous Integration and Continuous Delivery 

Deploying Jenkins! 

### Summary: Jenkins ###
Version: Alpha Release 0.1 - Archimedes 

This guide is part of the Intro to CI presented by Edge. The goal of this guide is to 
take you through the steps to setup and deploy Jenkins and a Docker Container via an 
automated deployment process. 

This guide is intended for beginners. 

### Guide ###

[Docker](https://docs.docker.com/docker-for-windows/install/) and [Jenkins](https://jenkins.io/)

Have been pre-installed on your training Laptops. 

*Docker
To start Docker, search via the Windows search function for "Docker Desktop" 

*Jenkins 
Jenkins can be accessed [here](http://localhost:8080/)

Log into Jenkins with the credentials provided to you by your course instructor


From the Jenkins dashboard:

Step 1: Click on New Item to create a Job.

![Scheme](/Images/Jenkins_1.PNG)

Step 2: Select a **Freestyle** project and provide the item name, in this guides case 
I've used Edge Deployment

![Scheme](/Images/Jenkins_3.PNG)
![Scheme](/Images/Jenkins_2.PNG)

Step 3: Select **Source Code Management** and provide a repository url
in our case https://bitbucket.org/leercourt/intro-to-ci3/src/<yourbranch>/
Make sure you are using your branch!
Then click on **Apply** and **Save**. 

![Scheme](/Images/Jenkins_4.PNG)

Step 4: Then click on **Build-> Select Execute Shell**

![Scheme](/Images/Jenkins_5.PNG)

Step 5: Enter the shell command shown in the picture, this will build a war file. 
This will trigger maven to install and build all of the dependences and compile our application.
Our application is a Java Script web page and a JUnit Unit test that will verify that 
our build has been successful.After adding the code select **Apply** a **Save Completed**
message will appear. Once it does select the Jenkins Logo in the web browser which will 
return you to the dashboard.

![Scheme](/Images/Jenkins_6.PNG)

Step 6: Select **New Item** and then select **Freestyle project** under **Enter an item 
name** enter **Edge Deployment 2**

![Scheme](/Images/Jenkins_7.PNG)! 
![Scheme](/Images/Jenkins_8.PNG)

Step 7: Once again select **Source Code Management**, **Git** and enter the intro to 
ci BitBucket URL https://bitbucket.org/leercourt/intro-to-ci3/src/<yourbranch>/

![Scheme](/Images/Jenkins_9.PNG)

Step 8: Then click on **Build->Select** Execute Shell. 

![Scheme](/Images/Jenkins_10.PNG)

Step 9: Enter the Shell code into the **Execute Shell** command box as show in the picture. 
Then select **Apply** and **Save**

![Scheme](/Images/Jenkins_11.PNG)

Step 10: Create another **New Item** and as before select **Freestyle** and enter an 
item name called **Edge Deployment 3**

Step 11: Select **Source Code Management** and provide the **Git** respository:https://bitbucket.org/leercourt/intro-to-ci3/src/<yourbranch>/ . 

Step 12: Then click on Build->Select Execute Shell

Step 13: This step will verify that a Docker Container file exists and deploys is to 
port 8090. Write the Shell script as shown in the picture:
![Scheme](/Images/Jenkins_13.PNG)

Step 14: Now select **Edge Deployment** then **Configure**

Step 15: Click on **Post-build Actions -> Build other projects.**
![Scheme](/Images/Jenkins_14.PNG)

Step 16: Now provide the name of the task to be carried out after **Edge Deployment** 
in our case thats **Edge Deployment 2** select save, return to the dashboard.

Step 17: Select **Edge Deployment 2** followed by **Configure**

Step 18: Click on **Post-build Actions -> Build other projects.**

Step 19: Under **Projects to build** enter **Edge Deployment3** by  this point I hope 
your noticing a pattern, Deployment 1, triggers Deployment 2 and Deployment 2 will trigger 
Deployment 3.

Step 20: From the dashboard we will now create a pipeline, select the plus arrow as 
show in the picture:

![Scheme](/Images/Jenkins_15.PNG)

Step 21: Select **Build Pipeline View** and provide the view name **CI CD Pipeline**



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Guide Owner ###

Edge Academy is headed up by;
Duncan Small
Email Address: duncan.small@edgetesting.co.uk 

For support please email:
Email Address: Edge Helpdesk <support@edgetesting.zendesk.com>

Technical Support:
Lee Court
Email Address: lee.court@edgetesting.co.uk